#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#include <sys/mman.h>
#include <stdint.h>
#include <inttypes.h>

#include <dlfcn.h>

/* The "volatile" is due to gcc bugs */
#define io_uring_barrier()	__asm__ __volatile__("": : :"memory")
#define io_uring_mb()		asm volatile("mfence" ::: "memory")
#define io_uring_rmb()		asm volatile("lfence" ::: "memory")
#define io_uring_wmb()		asm volatile("sfence" ::: "memory")
#define io_uring_smp_rmb()	io_uring_barrier()
#define io_uring_smp_wmb()	io_uring_barrier()

struct perf_record_sample {
	struct perf_event_header header;
	uint64_t nr;
	uint64_t ips[];
};

long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags)
{
	int ret;
	ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
	               group_fd, flags);
	return ret;
}

void print_stack(struct perf_record_sample *prs)
{
	uint64_t i;

	for (i = 0; i < prs->nr; i++) {
		printf("0x%lx\n", prs->ips[i]);
	}
	printf("\n");
}


#define PERF_HEADER_ENTRY(name)  \
case name: \
	printf(#name " "); \
	break;


void perf_print_header_type(struct perf_event_header *header)
{
	printf(" - entry: ");
	switch (header->type) {
	PERF_HEADER_ENTRY(PERF_RECORD_MMAP)
	PERF_HEADER_ENTRY(PERF_RECORD_LOST)
	PERF_HEADER_ENTRY(PERF_RECORD_COMM)
	PERF_HEADER_ENTRY(PERF_RECORD_EXIT)
	PERF_HEADER_ENTRY(PERF_RECORD_THROTTLE)
	PERF_HEADER_ENTRY(PERF_RECORD_UNTHROTTLE)
	PERF_HEADER_ENTRY(PERF_RECORD_FORK)
	PERF_HEADER_ENTRY(PERF_RECORD_READ)
	PERF_HEADER_ENTRY(PERF_RECORD_SAMPLE)
	default:
		printf("[type %d not known]", header->type);
	}

	switch (header->misc & PERF_RECORD_MISC_CPUMODE_MASK) {
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_CPUMODE_UNKNOWN)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_KERNEL)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_USER)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_HYPERVISOR)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_GUEST_KERNEL)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_GUEST_USER)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_MMAP_DATA)
	default:
		printf("[misc %d not known]", header->misc);
	}

	if (header->misc & PERF_RECORD_MISC_MMAP_DATA)
		printf("PERF_RECORD_MISC_MMAP_DATA");
	else if (header->misc & PERF_RECORD_MISC_EXACT_IP)
		printf("PERF_RECORD_MISC_EXACT_IP");
	else if (header->misc & PERF_RECORD_MISC_EXT_RESERVED)
		printf("PERF_RECORD_MISC_EXT_RESERVED");

	printf("\n");
	printf(" - header size: %"PRIu16"\n", header->size);
}

int main(int argc, char **argv)
{
	struct perf_event_mmap_page *mpage;
	struct perf_event_header *header;
	struct perf_event_attr pe;
	struct perf_record_sample *prs;
	char *dmpage;
	long long count;
	size_t n, tsize, dsize;
	size_t i;
	int fd;
	unsigned long long int hi, ti, ci;
	unsigned long long int cnt;

	char *addr;
	size_t msize;

	memset(&pe, 0, sizeof(struct perf_event_attr));
	//pe.type = PERF_TYPE_HARDWARE;
	//pe.config = PERF_COUNT_HW_INSTRUCTIONS;

	pe.size = sizeof(struct perf_event_attr);
	//pe.exclude_callchain_user = 1;
	pe.freq = 1;
	pe.sample_freq = 999;
	pe.sample_type = PERF_SAMPLE_CALLCHAIN;

	fd = perf_event_open(&pe, 0, -1, -1, 0);
	if (fd == -1) {
		perror("Error calling perf_event_open");
		return 1;
	}

	dsize = 2*4*1024;
	tsize = 4*1024 + dsize;

	mpage = mmap(NULL, tsize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if (mpage == (struct perf_event_mmap_page *)-1L) {
		close(fd);
		return -1;
	}

	msize = 1*1024*1024*1024UL;
	addr = malloc(msize);
	if (!addr) {
		perror("cannot allocate memory");
		return 1;
	}
	for (i = 0; i < msize; i++)
		addr[i] = '3';
	free(addr);

	dmpage = ((char *)mpage) + 4*1024UL;

	hi = mpage->data_head;
	ti = mpage->data_tail;
	ci = ti;
	io_uring_rmb();
	cnt = 0;
	while (ci != hi && cnt < 100000) {
		//printf("head:%lu tail:%lu ci:%lu\n", hi, ti, ci);
		header = (struct perf_event_header *) (dmpage + (ci % dsize));
		//perf_print_header_type(header);

		if (header->type == PERF_RECORD_SAMPLE) {
			prs = (struct perf_record_sample *) header;
			// WARNING THE HEAD->size and calculated record size
			// does not match! a 8 byte field is missing...
			print_stack(prs);
		}

		ci += header->size;
		hi = mpage->data_head;
		io_uring_rmb();
		cnt++;
	}

	munmap(mpage, tsize);
	close(fd);
}

