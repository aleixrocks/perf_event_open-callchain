#!/bin/bash

RED='\033[33m'
NC='\033[0m' # No Color

awk '
	BEGIN{
		e = 1
		CMD = "addr2line -e /boot/vmlinuz-3.10.0-957.el7.x86_64  -fpia"
	}

	$1 != "" && e == 1 {
		system(CMD " " $1)
		next
	}

	$1 == "" && e == 1 {
		print ""
		next
	}
' | awk '
     $1 == "(inlined" {
     	val = $3
     	$1 = $2 = $3 = ""
     	print "(inlinedby)         " "'$RED'"val"'$NC'" " " $0
     	next
     }
     $0 != "" {
     	first = $1
     	second = $2
     	$1 = ""
     	$2 = ""

     	print first " " "'$RED'"second"'$NC'" " " $0
	next
     }
     {
	print ""
     }
'
